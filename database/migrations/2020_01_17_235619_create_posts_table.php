<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger("status")->default(0);
            $table->bigInteger("author_id",false,true);
            $table->bigInteger("moderator_id",false,true)->nullable();
            $table->bigInteger("category_id",false,true);
            $table->tinyInteger("type",false,true);
            $table->tinyInteger("public",false,true);

            $table->float("position",false,true)->default(0);

            $table->string("title");
            $table->longText("body");
            $table->timestamps();

            $table->foreign("author_id")->references('id')->on('users');
            $table->foreign("moderator_id")->references('id')->on('users');
            $table->foreign("category_id")->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
