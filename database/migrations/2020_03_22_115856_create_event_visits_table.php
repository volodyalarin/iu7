<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("event_id",false,true);
            $table->bigInteger("user_id",false,true);


            $table->timestamps();

            $table->foreign("event_id")->references('id')->on('events');
            $table->foreign("user_id")->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_visits');
    }
}
