@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="col-12 col-md-3 shadow vertical-menu">
            <tree-view></tree-view>
        </div>
        <div class="col-12 col-md-9">

        </div>
    </div>

    <style>
        .vertical-menu {
            position: sticky;
            top: 0;
            min-height: 200px;
            max-height: 100vh;
            overflow: auto;
            margin-top: -23px;
        }
    </style>

@endsection

