import {QAvatar, QLayout, Quasar} from "quasar";

require('./bootstrap');
require("bootstrap")

import Vue from "vue";
import TreeView from "./components/TreeView";

import 'quasar'
new Vue({
    el: "#app",
    data:{
        left:true
    },
    components: {
        TreeView,
        QLayout
    },
    plugins:{
        Quasar
    }
});
