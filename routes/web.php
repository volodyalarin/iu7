<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;

Route::get("/testing-30-03-2020-wqld732o", function() {



});

Route::get("/update-25-03-2020-weu9sw732o", function(){
    foreach (\App\User::all() as $user){
        $login = substr($user->login, -6);
        $login = str_replace("u", "У", $login);
        $user->contingent_login = $login;
        $user->save();
        echo $login . "\n";
    }
    return "Updated";
});

Route::fallback(function (Request $request){
    $url = $request->getPathInfo();
    $full_file_url = '../quasar/dist/pwa'.$url;

    $ext = array_reverse(explode(".", basename($url)))[0];
    $mimies = [
        "js" => "application/javascript",
        "json" => "application/json",
        "css" => "text/css",
        "html" => "text/html"
    ];
    if(is_file($full_file_url)){
        return response()->file($full_file_url,[
            'Content-Type'=> isset($mimies[$ext]) ? $mimies[$ext] : null
        ]);
    }else{
        return response()->file('../quasar/dist/pwa/index.html');
    }


});
