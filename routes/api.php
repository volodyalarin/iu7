<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get("/user", "UserController@get");
    Route::post("/posts/store", "PostController@store");
    Route::get("/posts/{post}", "PostController@show");


    Route::get("/files/many", "FileController@displayMany");
    Route::post("/files", "FileController@store");
    Route::get("/files/{file}", "FileController@show");
    Route::patch("/files/{file}", "FileController@update");
    Route::get("/files", "FileController@index");

    Route::namespace("Moderator")->prefix("moderator")->group(function(){
        Route::get("/posts", "PostController@index");
        Route::get("/posts/{post}", "PostController@show");
        Route::patch("/posts/{post}", "PostController@update");
        Route::patch("/posts", "PostController@massUpdate");
        Route::get("/categories", "CategoryController@index");
        Route::post("/categories", "CategoryController@store");
        Route::patch("/categories/{category}", "CategoryController@update");
        Route::patch("/categories", "CategoryController@massUpdate");
    });

    Route::namespace("Group")->prefix("group")->group(function() {
        Route::get("/rasp", "BmstuProxy@proxy");



        Route::get("/events/{event}/visit", "EventController@visit");
        Route::get("/events/{event}/visit", "EventController@visit");
        Route::get("/events/{event}/destroy", "EventController@destroy");
        Route::patch("/events/{event}", "EventController@update");
        Route::patch("/events/{event}/visits", "EventController@sync_visits");
        Route::get("/events", "EventController@index");
        Route::post("/events", "EventController@store");
        Route::get("/students", "EventController@students");

        Route::post("/events/sync/{date}", "BmstuProxy@sync");


        Route::get("/tasks", "TaskController@index");
        Route::post("/tasks", "TaskController@store");
        Route::patch("/tasks/{event}", "TaskController@update");
        Route::get("/tasks/{event}/destroy", "TaskController@destroy");

    });

    Route::post("user/setNotificationToken", "TokenController@setNotificationToken");

});

Route::get("/categories", "CategoryController@index");
Route::get("/time", function(){
    return response()->json([
        "time" => \Carbon\Carbon::now()
    ]);
});


Route::get('/cas', 'AuthCasController@auth');
Route::post('/cas', 'AuthCasController@auth');


Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found.'], 404);
});



