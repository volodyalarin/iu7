import axios from "../boot/axios";



const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      {path: '', component: () => import('pages/Index.vue')},
      {path: '/login', component: () => import('pages/auth/Login.vue')},
      {
        path: '/savetoken', beforeEnter: (to, from, next) => {
          if(to.query.api_token) {
            let token = to.query.api_token;
            localStorage.setItem('api_token', token);
            axios.setToken(token);
            return next('/')
          }
          next('/login')
        },
      },
      {
        path: '/logout', beforeEnter: (to, from, next) => {
          localStorage.setItem('api_token','');
          axios.setToken('');
          next('/');
          location.reload();
        },
      },
      {name:'cloud', path: '/cloud', component: () => import('pages/Cloud.vue')},
      {name:'publish', path: '/cloud/post/publish', component: () => import('pages/Publish.vue')},
      {name:'post', path: '/cloud/post/:post', component: () => import('pages/Post.vue')},
      {name:'file', path: '/cloud/file/:file', component: () => import('pages/file.vue')},
      {name:'moderator', path: '/moderator', component: () => import('pages/Moderator/index.vue')},
      {name:'moderator.posts', path: '/moderator/posts', component: () => import('pages/Moderator/Posts.vue')},
      {name:'moderator.post.edit', path: '/moderator/posts/:post', component: () => import('pages/Moderator/Post.vue')},
      {name:'moderator.categories', path: '/moderator/categories', component: () => import('pages/Moderator/Categories.vue')},

      {name:'group', path: '/group', component: () => import('pages/Group/Index.vue')},
      {name:'group.table', path: '/group/table/', component: () => import('pages/Group/Table.vue')},

      {name:'pool.answer', path: '/pools/answer/:pool', component: () => import('pages/Pool/Answer.vue')},
      {name:'pool.results', path: '/pools/results/:pool', component: () => import('pages/Pool/Result.vue')},

    ]
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
