import Vue from "vue";

import * as firebase from "firebase/app";

import "firebase/messaging";



const firebaseConfig = {
    apiKey: "AIzaSyDCd1txzVZ5JOTrT4M_RTKUwYOJ_tqLzsY",
    authDomain: "dynamic-chiller-215520.firebaseapp.com",
    databaseURL: "https://dynamic-chiller-215520.firebaseio.com",
    projectId: "dynamic-chiller-215520",
    storageBucket: "dynamic-chiller-215520.appspot.com",
    messagingSenderId: "355899234212",
    appId: "1:355899234212:web:259a220b646d6e4a6d9331"
};

try {

// Initialize Firebase
    firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
    const messaging = firebase.messaging();
    navigator.serviceWorker.register("/service-worker.js")
        .then((registration) => {
            messaging.useServiceWorker(registration);

            messaging.usePublicVapidKey("BGHZxlyBQZHhYKM_u0r8hO8yh55gE1eW-q9eOLXhobtFTJZps8tVSEtB1Rz9lNblBHqvZzMnnHgRVprGmmjbRMU");


            messaging.getToken().then((currentToken) => {
                if (currentToken) {
                    Vue.prototype.$store.dispatch('user/setNotificationToken', currentToken)

                } else {
                    console.log('No Instance ID token available. Request permission to generate one.');
                }
            }).catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
            });
        });
}
catch (e) {
    console.log("Firebase ERROR");
}
