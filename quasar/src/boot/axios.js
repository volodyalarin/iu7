import Vue from 'vue'
import axios from 'axios'

let instance = axios.create({
  baseURL:"https://iu7.larin.tech/api"
});
Vue.prototype.$axios = instance;

function setToken(token){
  instance.defaults.headers.common['Authorization'] = 'Bearer '+token;

}

if (localStorage.getItem('api_token')){
  setToken(localStorage.getItem('api_token'))
}
export default {
  setToken,
  instance
}

