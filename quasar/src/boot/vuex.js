import Vue from "vue";
import Store from "../store"

Vue.prototype.$store = Store();
export default Vue.prototype.$store;