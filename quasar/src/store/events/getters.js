/*
export function someGetter (state) {
}
*/

export function isActive(state) {
    if (!state.loaded) return false;
    return state.data.length > 0;
}