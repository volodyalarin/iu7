/*
export function someAction (context) {
}
*/

import axios from "../../boot/axios";
export async function update(context) {
    let res = await axios.instance.get('/group/events').catch(()=>{
        context.commit("update", {
            loaded: true
        });
    });
    if (res.data) {
        let data = {};
        data.loaded = true;
        data.data = res.data.data;
        context.commit("update", data);
    }
}
