/*
export function someAction (context) {
}
*/

import axios from "../../boot/axios";
export async function update(context) {
    let res = await axios.instance.get('/user').catch(()=>{
        context.commit("update", {
            loaded: true
        });
    });
    if (res.data) {
        let data = {};
        data.loaded = true;
        data.auth = true;
        data.data = res.data.data;
        context.commit("update", data);
    }
}

export async function setNotificationToken(context, token) {
    let res = await axios.instance.post('/user/setNotificationToken', {
        token: token
    });
    if (res.data) {
        let data = {
            use_notifications: true
        };
        context.commit("update", data);
    }
}
