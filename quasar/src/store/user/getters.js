/*
export function someGetter (state) {
}
*/
export function isModerator(state) {
    if (!state.auth) return false;
    return state.data.role > 100;
}

export function isLeader(state) {
    if (!state.auth) return false;
    return state.data.role > 20;
}