<?php

namespace App;

use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Event extends Model
{

    protected $fillable = [
        'name',
        'date',
        'speaker',
        'description',
        'group',
        'type',
        'theme'
    ];

    function visits()
    {
        return $this->hasMany(EventVisit::class);
    }

    public function setDateAttribute( $value ) {
        $this->attributes['date'] = (new Carbon($value))->format('Y-m-d H:i');
    }


    function contingent()
    {
        return $this->hasMany(Contingent::class, 'group', 'group');
    }

    function notifications_tokens(){
        $contingent = $this->contingent()->with("user", "user.notification_tokens")->get();

        $tokens = [];

        foreach ($contingent as $c)
        {
            if($c and $c->user){
                $tokens += $c->user->notification_tokens->map(function ($t) {
                    return $t->token;
                })->toArray();
            }
        }
        return $tokens;
    }



}
