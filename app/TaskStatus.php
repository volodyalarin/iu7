<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    protected $fillable = [
        "user_id",
        "task_id",
    ];

    function task(){
        return $this->belongsTo(Task::class);
    }

    function user(){
        return $this->belongsTo(User::class);
    }

}
