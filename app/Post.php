<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const statuses = [
        "draft" => -1,
        "in_moderation" => 0,
        "published" => 1
    ];
    protected $fillable = [
        'author_id', "moderator_id", "category_id", "type", "public", "position", "title", "body", "status"
    ];


    function author()
    {
        return $this->belongsTo(User::class, "author_id");
    }

    function moderator()
    {
        return $this->belongsTo(User::class, "moderator_id");
    }

    function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function files(){
        return $this->belongsToMany(File::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('position', 'asc');
        });
    }
}
