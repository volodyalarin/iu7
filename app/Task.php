<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        "group",
        "name",
        "subject",
        "description",
        "deadline",
    ];

    function statuses(){
        return $this->hasMany(TaskStatus::class);
    }
}
