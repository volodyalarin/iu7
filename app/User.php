<?php

namespace App;

use App\Http\Resources\PostsResource;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login','contingent_login' ,'password', 'role', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function username(){
        return "login";
    }
    public function posts(){
        return $this->hasMany(Post::class, 'author_id');
    }

    public function contingent(){
        return $this->hasOne(Contingent::class, "login", "contingent_login");
    }

    public function notification_tokens(){
        return $this->hasMany(NotificationToken::class);
    }


}
