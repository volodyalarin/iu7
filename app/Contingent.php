<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contingent extends Model
{
    protected $table = "iu7_contingent";

    public function user(){
        return $this->hasOne(User::class, "contingent_login", "login");
    }
}
