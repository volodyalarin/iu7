<?php

namespace App;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        "type", "public", "title", "parent_id", "position"
    ];

    function parent()
    {
        return $this->belongsTo(Category::class, "parent_id");
    }

    function children()
    {
        return $this->hasMany(Category::class, "parent_id");
    }

    function posts()
    {
        return $this->hasMany(Post::class);
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('position', 'asc');
        });
    }

}
