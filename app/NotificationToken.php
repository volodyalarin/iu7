<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationToken extends Model
{
    protected $fillable = [
        "token", "user_id"
    ];
    protected $primaryKey = "token";

    protected $keyType = 'string';
    public $incrementing = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

}
