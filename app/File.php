<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        "name", "type", "path", "size", "user_id",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function posts(){
        return $this->belongsToMany(Post::class);
    }

}
