<?php

namespace App\Http\Controllers;

use App\NotificationToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{
    public function setNotificationToken(Request $request){
        $request->validate([
            "token" => "required|string",
        ]);
        $token = $request->post('token');
        $ntoken = NotificationToken::find($token);
        if ($ntoken){
            $ntoken->update([
                "user_id"=>Auth::id()
            ]);
        }else{
            NotificationToken::create([
                "token" => $token,
                "user_id"=>Auth::id(),
            ]);
        }

        return response()->json([
            "status"=>"success"
        ]);
    }
}
