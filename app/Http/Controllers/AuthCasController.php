<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use function Psy\debug;
use Nathanmac\Utilities\Parser\Parser;

class AuthCasController extends Controller
{
    public function auth(Request $request)
    {
        $ticket = $request->get('ticket');
        if (!$ticket) {
            return response()->json([
                'success' => false,
                'description' => 'Неправильный тикет'
            ])->setStatusCode(401);
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://proxy.bmstu.ru:8443/cas/proxyValidate?service=https://iu7.larin.tech/api/cas&ticket=" . $ticket,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json([
                'success' => false,
                'description' => 'Произошла ошибка',
                'debug' => $err
            ])->setStatusCode(500);
        }
        $parser = new Parser();
        $response = $parser->xml($response);
        $username = "";
        if ($response['cas:authenticationSuccess']) {
            $username = $response['cas:authenticationSuccess']['cas:user'];
        } else {
            return response()->json([
                'success' => false,
                'description' => 'Произошла ошибка',
                'debug' => $response
            ])->setStatusCode(401);
        }

        $user = User::where([
            'login' => $username
        ])->first();

        if ($user) {
            if (!$user->api_token) {
                $user->api_token = Str::random(80);
                $user->save();
            }
        } else {
            $login = substr($username, -6);
            $login = str_replace("u", "У", $login);
            $iu7_contingent = DB::table('iu7_contingent')->where('login', 'like', $login)->first();
            $name = $username;
            if ($iu7_contingent) $name = $iu7_contingent->name . " " . $iu7_contingent->surname;
            $user = User::create([
                'login' => $username,
                'contingent_login' => $login,
                'name' => $name,
                'password' => Hash::make(Str::random(80)),
                'api_token' => Str::random(80),
            ]);
        }

        Auth::loginUsingId($user->id);
        return response()->redirectTo('https://xn--7-otb7a.xn--p1ai/savetoken?api_token=' . $user->api_token);


    }
}
