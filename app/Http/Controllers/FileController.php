<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Resources\FileResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return FileResource::collection(File::all());
    }

    /**
     * Display files by ids.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function displayMany(Request $request)
    {

        return FileResource::collection(File::whereIn("id",$request->get('ids'))->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return FileResource
     */
    public function store(Request $request)
    {
        $file_entity = $request->validate([
            "name" => "required|string|max:255",
            "file" => "required|file|max:102400",
        ]);


        $file = $request->file('file');
        $file_entity['type'] = $file->getClientOriginalExtension();
        $file_entity['size'] = $file->getSize();
        $file_entity['path'] = basename($file->store('/public/'));
        $file_entity['user_id'] = Auth::user()->id;
        $file_entity = File::create($file_entity);

        return new FileResource($file_entity);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\File $file
     * @return FileResource
     */
    public function show(File $file)
    {

        return new FileResource($file);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\File $file
     * @return FileResource
     */
    public function update(Request $request, File $file)
    {
        $file_entity = $request->validate([
            "name" => "required|string|max:255",
        ]);

        $file->update($file_entity);

        return new FileResource($file);
    }
}
