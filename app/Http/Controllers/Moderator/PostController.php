<?php
namespace App\Http\Controllers\Moderator;

use App\Http\Controllers\Controller;
use App\Http\Resources\Moderator\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PostResource::collection(Post::all());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return PostResource
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Post $post)
    {
        $post->update($request->validate([
            "category_id" => "integer|exists:categories,id",
            "type" => "integer",
            "title" => "string|max:255",
            "body" => "string",
            "files.*" => "integer|exists:files,id",
            "public" => "bool",
            "status" => "integer",

        ]));
        $post->files()->sync($request->post('files'));
        return (new PostResource($post))->response();
    }

    public function massUpdate(Request $request)
    {
        $data = $request->json();
        foreach ($data as $category) {
            Post::where([
                "id" => $category['id']
            ])->update($category);
        }
        return response()->json([
            "success" => true
        ]);
    }
}