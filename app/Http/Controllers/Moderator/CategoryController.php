<?php

namespace App\Http\Controllers\Moderator;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriesResource;
use App\Http\Resources\Moderator\CategoryResource;
use App\Http\Resources\Moderator\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoriesResource::collection(Category::whereNull("parent_id")->get());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return PostResource
     */
    public function show(Post $post)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->validate([
            "parent_id" => "integer|exists:categories,id|nullable",
            "type" => "integer",
            "title" => "string|max:255",
            "public" => "bool",

        ]));
        return (new CategoryResource($category))->response();
    }

    public function massUpdate(Request $request)
    {
        $data = $request->json();
        foreach ($data as $category) {
            Category::where([
                "id" => $category['id']
            ])->update($category);
        }
        return response()->json([
            "success" => true
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $category = new Category($request->validate([
            "parent_id" => "integer|exists:categories,id|nullable",
            "type" => "integer",
            "title" => "string|max:255",
            "public" => "bool",
        ]));
        $category->public = true;
        $category->type = 1;
        $category->position = 0;
        $category->save();
        return (new CategoryResource($category))->response();

    }


}