<?php

namespace App\Http\Controllers;

use App\Http\Resources\FullUserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function get(){
        return new FullUserResource(Auth::user());
    }
}
