<?php

namespace App\Http\Controllers\Group;

use App\Event;
use App\EventVisit;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventResourse;
use App\Http\Resources\TaskResource;
use App\Http\Resources\UserResource;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = Auth::user()->contingent->group;

        return TaskResource::collection(Task::where("group", $group)->orderBy("deadline")->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    public function store(Request $request)
    {
        $group = Auth::user()->contingent->group;

        $task = $request->validate([
            "name" => "string | max:255",
            "subject" => "string | max:255",
            "description" => "nullable | string",
            "deadline" => "date"
        ]);

        $task = new Task($task);

        $task->group = $group;

        $task->save();

        return response()->json([
            "status" => "success",
            "task" => new TaskResource($task)
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Event $event
     * @return
     */
    public function update(Request $request, Task $task)
    {
        $task->update($request->validate([
            "name" => "string | max:255",
            "subject" => "string | max:255",
            "description" => "nullable | string",
            "deadline" => "date"
        ]));

        return response()->json([
            "status" => "success"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Event $event
     * @return
     */
    public function destroy(Task $task)
    {
        $task->statuses()->delete();
        $task->delete();
        return response()->json([
            "status" => "success"
        ]);

    }

}
