<?php

namespace App\Http\Controllers\Group;

use App\Event;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Psy\Util\Json;
use function foo\func;
use function Psy\debug;
use Nathanmac\Utilities\Parser\Parser;


class BmstuProxy extends Controller
{
    public function proxy()
    {
        $schedule = BmstuProxy::load_schedule();

        if ($schedule["success"])
            return response()->json($schedule["response"]);

        return response()->json($schedule)->setStatusCode(500);
    }

    public function sync($date)
    {


        $schedule = BmstuProxy::load_schedule();

        if (!$schedule["success"])
            return response()->json($schedule)->setStatusCode(500);
        $data = $schedule["response"]->data;

        $first_day_in_semester = Carbon::parse($data->first_day_in_semester);
        $date = Carbon::parse($date);

        $week = ($date->diffInWeeks($first_day_in_semester) + 1) % 2 ? "at_numerator" : "at_denominator";
        $weekDay = ($date->dayOfWeek + 6) % 7;

        if ($week)
            $day_schedule = $data->schedule_days[$weekDay]->at_numerator;
        else
            $day_schedule = $data->schedule_days[$weekDay]->at_denominator;

        $events = [];
        foreach ($day_schedule as $event_sh) {


            $date->hour = $event_sh->time->from_h;
            $date->minute = $event_sh->time->from_m;

            $event = Event::create([
                "name" => $event_sh->subject,
                "speaker" => $event_sh->person,
                "date" => $date,
                "group" => Auth::user()->contingent->group,
                "type" => str_replace([
                    "(сем)",
                    "(лаб)",
                    "(лек)"
                ], [
                    "Семинар",
                    "Лабораторная работа",
                    "Лекция"
                ], $event_sh->prefix)
            ]);
            $events[] = $event;

        }

        return response()->json([
            "success" => true,
            "events"=> $events
        ]);
    }


    function translit($textcyr)
    {

        $cyr = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            'A', 'B', 'V', 'G', 'D', 'E', 'Io', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P',
            'R', 'S', 'T', 'U', 'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'A', 'I', 'Y', 'e', 'Yu', 'Ya'
        ];
        $textcyr = str_replace($cyr, $lat, $textcyr);

        return $textcyr;
    }

    public function load_schedule()
    {
        $group = BmstuProxy::translit(Auth::user()->contingent->group);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://b.bmstu.ru/api/schedule/" . $group,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);


        if ($err)
            return [
                "success" => false,
                "err" => $err
            ];

        return [
            "success" => true,
            "response" => json_decode($response)
        ];
    }

}

