<?php

namespace App\Http\Controllers\Group;

use App\Event;
use App\EventVisit;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventResourse;
use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = Auth::user()->contingent->group;

        return EventResourse::collection(Event::where("group", $group)->orderBy("date")->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    public function store(Request $request)
    {
        $group = Auth::user()->contingent->group;

        $event = $request->validate([
            "speaker" => "string | max:255",
            "date" => "date | after: now",
            "name" => "string | max:255",
            "type" => "string | max:255",
            "description" => "nullable | string",
            "theme" => "nullable | string | max:255 "
        ]);

        $event = new Event($event);

        $event->group = $group;

        $event->save();

        return response()->json([
            "status" => "success",
            "event" => $event
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Event $event
     * @return
     */
    public function update(Request $request, Event $event)
    {
        $event->update($request->validate([
            "speaker" => "string | max:255",
            "date" => "date",
            "name" => "string | max:255",
            "type" => "string | max:255",
            "description" => "nullable | string",
            "theme" => "nullable | string | max:255 "
        ]));

        return response()->json([
            "status" => "success"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Event $event
     * @return
     */
    public function destroy(Event $event)
    {
        $event->visits()->delete();
        $event->delete();
        return response()->json([
            "status" => "success"
        ]);

    }


    /**
     *
     *
     * @param \App\Event $event
     * @return
     */
    public function visit(Event $event)
    {

        $date =new Carbon($event->date, 3);
        $diff = $date->diffInMinutes(Carbon::now(), false);

        if ($diff > 30 || $diff < -5)
            return response()->json([
                "status" => "false",
                "event" => $event
            ])->setStatusCode(403);
        $visit_data = [
            "user_id" => Auth::id(),
            "event_id" => $event->id
        ];

        $visit = EventVisit::where($visit_data)->first();

        if ($visit) {
            return response()->json([
                "status" => "success",
                "visit" => $visit,
                "event" => $event
            ]);
        }
        $visit = EventVisit::create($visit_data);

        return response()->json([
            "status" => "success",
            "visit" => $visit,
            "event" => $event
        ]);
    }

    public function sync_visits(Event $event, Request $request)
    {

        $data = $request->validate([
            "students" => "array",
            "students.*" => "exists:users,id"
        ]);

        $event->visits()->delete();
        $event->visits()->createMany(array_map(function ($id){
            return [
                "user_id" => $id
            ];
        }, $data["students"]));

        return response()->json([
            "status" => "success",
            "event" => $event
        ]);
    }

    public function students()
    {
        $group = Auth::user()->contingent->group;
        return UserResource::collection(User::whereHas('contingent', function($query) use ($group) {
            return $query->where('group', $group);
        })->get());
    }

}
