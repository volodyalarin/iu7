<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "date" => new Carbon($this->date, 3),
            "speaker" => $this->speaker,
            "type" => $this->type,
            "description" => $this->description,
            "theme" => $this->theme,
            "group" => $this->group,
            "visits" => EventVisitResourse::collection($this->visits)
        ];
    }
}
