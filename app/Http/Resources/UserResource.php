<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name"=> $this->name,
            "avatar" => "https://secure.gravatar.com/avatar/".sha1($this->login."@student.bmstu.ru")."?s=200&d=identicon"
        ];
    }
}
