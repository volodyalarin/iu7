<?php

namespace App\Http\Resources;

use App\Post;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoriesResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "children" => CategoriesResource::collection($this->children),
            "posts" => PostsResource::collection($this->posts()->where([
                "status"=>Post::statuses["published"]
            ])->get())

        ];
    }
}
