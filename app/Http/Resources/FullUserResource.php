<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FullUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "login" => $this->login,
            "name" => $this->name,
            "role" => $this->role,
            "posts" => PostsResource::collection($this->posts),
            "avatar" => "https://secure.gravatar.com/avatar/".sha1($this->login."@student.bmstu.ru")."?s=200&d=identicon",
            "contingent" => $this->contingent
        ];
    }
}
