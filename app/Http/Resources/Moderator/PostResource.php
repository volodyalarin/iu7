<?php

namespace App\Http\Resources\Moderator;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            'author' => new UserResource($this->author),
            "moderator" => new UserResource($this->moderator),
            "category_id" => $this->category_id,
            "category" => new CategoryResource($this->category),
            "type" => $this->type,
            "public" => $this->public,
            "position" => $this->position,
            "title" => $this->title,
            "body" => $this->body,
            "status" => $this->status,
            "created_at" => $this->created_at,
            "files" => $this->files,

        ];
    }
}
