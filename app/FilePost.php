<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FilePost extends Pivot
{
    protected $fillable = [
        "file_id", "post_id"
    ];
}
