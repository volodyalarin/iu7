<?php

namespace App\Console\Commands;

use App\Event;
use App\NotificationToken;
use Illuminate\Console\Command;

class PushNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notificate users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    }
}
