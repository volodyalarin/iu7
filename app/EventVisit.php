<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventVisit extends Model
{
    protected $fillable = [
       "event_id",
       "user_id"
    ];

    function event()
    {
        return $this->belongsTo(Event::class);
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
